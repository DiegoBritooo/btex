package com.btexdm.aluno.btex.Fragments.Login.Handlers;

import com.btexdm.aluno.btex.Fragments.Login.Models.User;
import com.btexdm.aluno.btex.Fragments.Login.Models.UserAchievement;
import com.btexdm.aluno.btex.Fragments.Notifications.NotificationHandler;
import com.btexdm.aluno.btex.MainActivity;

public class UserHandler {

    private static UserHandler instance;

    private User user;

    public static void setInstance(UserHandler instance) {
        UserHandler.instance = instance;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {

        if(user != null && this.user != null) {
            for (UserAchievement achievement : user.getAchievements()) {
                if(!this.user.isAchievementComplete(achievement.getId())) {
                    NotificationHandler.achievementNotification(achievement);
                }
            }

        }

        this.user = user;
        if(MainActivity.getInstance() != null && user != null) {
            MainActivity.getInstance().setBitecos(user.getBiteco());
        }
    }

    public String getUserToken() {
        if(user != null) {
            return "Bearer " + user.getToken();
        }
        return "";
    }

    public static UserHandler getInstance() {
        if(instance == null)
            instance = new UserHandler();
        return instance;
    }
}
