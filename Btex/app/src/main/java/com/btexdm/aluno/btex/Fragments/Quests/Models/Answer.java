package com.btexdm.aluno.btex.Fragments.Quests.Models;

public class Answer {

    private Integer index;

    private String text;

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
