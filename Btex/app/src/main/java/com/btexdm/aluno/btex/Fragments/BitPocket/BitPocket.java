
package com.btexdm.aluno.btex.Fragments.BitPocket;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.btexdm.aluno.btex.Fragments.BitPocket.adapters.BitPocketAchievementAdapter;
import com.btexdm.aluno.btex.Fragments.BitPocket.models.Achievement;
import com.btexdm.aluno.btex.Fragments.BitPocket.models.AchivementList;
import com.btexdm.aluno.btex.Fragments.BitPocket.models.Item_bit_pocket;
import com.btexdm.aluno.btex.Fragments.Login.Handlers.UserHandler;
import com.btexdm.aluno.btex.MainActivity;
import com.btexdm.aluno.btex.R;
import com.btexdm.aluno.btex.RetrofitConfig.RetrofitConfig;

import java.util.List;

import com.btexdm.aluno.btex.Utils.ErrorUtils;
import com.btexdm.aluno.btex.Utils.VibratorUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import steelkiwi.com.library.DotsLoaderView;

public class BitPocket extends Fragment {

    private GridView gridView;
    private View view;

    private DotsLoaderView dotsLoaderView;

    private ConstraintLayout wrapper;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_bit_pocket, container, false);
        gridView = (GridView) view.findViewById(R.id.grid);
        dotsLoaderView = view.findViewById(R.id.dotsLoader);
        wrapper = view.findViewById(R.id.wrapper);
        onLoad();
        return view;
    }

    public void onLoad() {
        startLoading();
        Call<AchivementList> call = new RetrofitConfig().getAchievements().doRequest(UserHandler.getInstance().getUserToken());

        call.enqueue(new Callback<AchivementList>() {

            @Override
            public void onResponse(Call<AchivementList> call, Response<AchivementList> response) {
                stopLoading();
                List<Achievement> items = response.body().getItems();

                Achievement[] achievements = items.toArray(new Achievement[items.size()]);

                if (response.code() != 200) {

                    android.support.v7.app.AlertDialog.Builder responseAlert = new android.support.v7.app.AlertDialog.Builder(getContext());
                    responseAlert.setTitle("Ops! Algo deu Errado :(");
                    responseAlert.setMessage(ErrorUtils.getErrorMessage(response.errorBody()));
                    responseAlert.setNeutralButton("Entendido", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            return;
                        }
                    });

                    responseAlert.show();

                } else {
                    BitPocketAchievementAdapter gridAdapater = new BitPocketAchievementAdapter(view.getContext(), achievements);

                    gridView.setAdapter(gridAdapater);

                    gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                            Achievement achievement = (Achievement) adapterView.getItemAtPosition(position);

                            if(UserHandler.getInstance().getUser().isAchievementComplete(achievement.getKey())) {
                                Fragment item_pocket = new Item_bit_pocket();
                                Bundle bundle = new Bundle();
                                bundle.putString("title", achievement.getTitle());
                                bundle.putString("description", achievement.getDescription());
                                bundle.putString("icon", achievement.getIcon());

                                item_pocket.setArguments(bundle);

                                MainActivity.getInstance().loadFragment(item_pocket);
                            }else{
                                VibratorUtils.vibrate(300);
                            }
                        }
                    });
                }
            }


            @Override
            public void onFailure(Call<AchivementList> call, Throwable t) {

                AlertDialog.Builder alertFailure = new AlertDialog.Builder(getContext());

                alertFailure.setTitle("Erro ao buscar conquistas");

                alertFailure.setMessage("Algo deu errado, verifique sua internet e tente novamente mais tarde.");

            }
        });

    }

    public void startLoading() {
        dotsLoaderView.show();
        wrapper.setVisibility(View.INVISIBLE);
    }

    public void stopLoading() {
        dotsLoaderView.hide();
        wrapper.setVisibility(View.VISIBLE);
    }

}
