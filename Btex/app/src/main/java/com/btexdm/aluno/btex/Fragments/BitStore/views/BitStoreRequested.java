package com.btexdm.aluno.btex.Fragments.BitStore.views;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.btexdm.aluno.btex.Fragments.BitStore.models.BuyItemAction;
import com.btexdm.aluno.btex.Fragments.Login.Handlers.UserHandler;
import com.btexdm.aluno.btex.Fragments.Login.Models.User;
import com.btexdm.aluno.btex.Fragments.Quests.Models.Quest;
import com.btexdm.aluno.btex.Fragments.Quests.Views.MainQuestsFragment;
import com.btexdm.aluno.btex.MainActivity;
import com.btexdm.aluno.btex.R;
import com.btexdm.aluno.btex.RetrofitConfig.RetrofitConfig;
import com.btexdm.aluno.btex.Utils.ErrorUtils;
import com.btexdm.aluno.btex.common.model.BTeXFragment;
import com.btexdm.aluno.btex.common.model.response.GenericListResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import steelkiwi.com.library.DotsLoaderView;

public class BitStoreRequested extends BTeXFragment {

    private View view;

    private TextView description;
    private TextView reward;
    private TextView txtMiddle;

    private Button start;

    private DotsLoaderView dotsLoaderView;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_bit_store_requested, container, false);

        description = view.findViewById(R.id.txtMiddle);
        reward = view.findViewById(R.id.reward);
        txtMiddle = view.findViewById(R.id.txtMiddle);

        start = view.findViewById(R.id.start);

        dotsLoaderView = view.findViewById(R.id.dotsLoader);

        onLoad();
        return view;
    }


    public void onLoad() {
        startLoading();

        BuyItemAction buyItemAction = new BuyItemAction();
        buyItemAction.setId(getArguments().getString("key"));

        Call<User> call = new RetrofitConfig().getStoreItens().buy(UserHandler.getInstance().getUserToken(), buyItemAction);

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(!response.isSuccessful()) {
                    AlertDialog.Builder responseAlert = new AlertDialog.Builder(view.getContext());
                    responseAlert.setTitle("Ops! Algo deu Errado :(");
                    responseAlert.setMessage(ErrorUtils.getErrorMessage(response.errorBody()));
                    responseAlert.setNeutralButton("Chamarei um professor", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            return;
                        }
                    });
                    responseAlert.show();
                    return;
                }

                stopLoading();

                UserHandler.getInstance().setUser(response.body());

                description.setText("Você efetuou a compra do item '" + getArguments().getString("title") + "' com sucesso.");
                reward.setText(String.format("- %.0f bitecos", getArguments().getDouble("cost")));

                start.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        MainActivity.getInstance().loadFragment(new BitStoreReservationsFragment());
                    }
                });
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                AlertDialog.Builder responseAlert = new AlertDialog.Builder(view.getContext());
                responseAlert.setTitle("Ops! Algo deu Errado :(");
                responseAlert.setMessage(t.getLocalizedMessage());
                responseAlert.setNeutralButton("Chamarei um professor", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                });
                responseAlert.show();
            }
        });

    }

    @Override
    public void onBackPressed() {
        MainActivity.getInstance().loadFragment(new MainQuestsFragment());
    }

    public void startLoading() {
        dotsLoaderView.show();
        description.setVisibility(View.INVISIBLE);
        reward.setVisibility(View.INVISIBLE);
        start.setVisibility(View.INVISIBLE);
        txtMiddle.setVisibility(View.INVISIBLE);
    }


    public void stopLoading() {
        dotsLoaderView.hide();
        description.setVisibility(View.VISIBLE);
        reward.setVisibility(View.VISIBLE);
        start.setVisibility(View.VISIBLE);
        txtMiddle.setVisibility(View.VISIBLE);
    }
}
