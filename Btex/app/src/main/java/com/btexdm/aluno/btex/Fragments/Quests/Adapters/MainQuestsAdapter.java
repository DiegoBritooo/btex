package com.btexdm.aluno.btex.Fragments.Quests.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.btexdm.aluno.btex.Fragments.Login.Handlers.UserHandler;
import com.btexdm.aluno.btex.Fragments.Quests.Models.Quest;
import com.btexdm.aluno.btex.R;
import com.squareup.picasso.Picasso;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class MainQuestsAdapter extends ArrayAdapter<Quest> {

    private final Context context;
    private final Quest[] values;

    public MainQuestsAdapter(Context context, Quest[] values) {
        super(context, R.layout.fragment_item_main_quests, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.fragment_item_main_quests, parent, false);

        Quest mainQuestItem = values[position];

        TextView title = (TextView) rowView.findViewById(R.id.titleMainQuests);
        TextView endDate = (TextView) rowView.findViewById(R.id.questEndDate);
        ImageView background = (ImageView) rowView.findViewById(R.id.mainQuestBackground);
        ConstraintLayout layout = rowView.findViewById(R.id.layout);
        ImageView done = rowView.findViewById(R.id.questFinished);

        if(UserHandler.getInstance().getUser().isMainQuestComplete(mainQuestItem.getKey())) {
//            layout.setBackgroundColor(Color.parseColor("#6D55efc4"));
            done.setVisibility(View.VISIBLE);
            title.setPadding(10,0,0,0);
        }

        Timestamp ts = new Timestamp(Long.parseLong(mainQuestItem.getEndDate()));

        String current = new SimpleDateFormat("MM.dd.yyyy").format(ts.getTime());

        endDate.setText("Encerra em: " + current);

        Picasso.get().load(mainQuestItem.getBackground()).into(background);

        title.setText(mainQuestItem.getTitle());

        return rowView;
    }

}
