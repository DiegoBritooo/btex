package com.btexdm.aluno.btex.Fragments.Login.Models;

import java.util.List;

public class UserCompleteQuests {

    private List<UserCompleteQuest> MAIN_QUESTS;
    private List<UserCompleteQuest> SIDE_QUESTS;

    public List<UserCompleteQuest> getMAIN_QUESTS() {
        return MAIN_QUESTS;
    }

    public void setMAIN_QUESTS(List<UserCompleteQuest> MAIN_QUESTS) {
        this.MAIN_QUESTS = MAIN_QUESTS;
    }

    public List<UserCompleteQuest> getSIDE_QUESTS() {
        return SIDE_QUESTS;
    }

    public void setSIDE_QUESTS(List<UserCompleteQuest> SIDE_QUESTS) {
        this.SIDE_QUESTS = SIDE_QUESTS;
    }
}
