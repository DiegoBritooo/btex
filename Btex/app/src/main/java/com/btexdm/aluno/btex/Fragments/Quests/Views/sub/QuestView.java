package com.btexdm.aluno.btex.Fragments.Quests.Views.sub;

import android.content.DialogInterface;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.btexdm.aluno.btex.Fragments.Login.Activity.LoginActivity;
import com.btexdm.aluno.btex.Fragments.Login.Handlers.UserHandler;
import com.btexdm.aluno.btex.Fragments.Login.Models.User;
import com.btexdm.aluno.btex.Fragments.Quests.Handlers.MainQuestHandler;
import com.btexdm.aluno.btex.Fragments.Quests.Handlers.SideQuestHandler;
import com.btexdm.aluno.btex.Fragments.Quests.Handlers.pattern.QuestHandler;
import com.btexdm.aluno.btex.Fragments.Quests.Models.Quest;
import com.btexdm.aluno.btex.Fragments.Quests.Models.request.QRCodeValidation;
import com.btexdm.aluno.btex.Fragments.Quests.Views.MainQuestsFragment;
import com.btexdm.aluno.btex.Fragments.Quests.Views.SideQuestsFragment;
import com.btexdm.aluno.btex.MainActivity;
import com.btexdm.aluno.btex.R;
import com.btexdm.aluno.btex.RetrofitConfig.RetrofitConfig;
import com.btexdm.aluno.btex.Utils.ErrorUtils;
import com.btexdm.aluno.btex.common.model.BTeXFragment;
import com.btexdm.aluno.btex.common.model.QRCodeResponse;
import com.google.zxing.integration.android.IntentResult;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import steelkiwi.com.library.DotsLoaderView;

public class QuestView extends BTeXFragment {

    private View view;

    private Quest quest;

    private TextView title;
    private TextView reward;
    private TextView description;

    private Button button;

    private DotsLoaderView dotsLoaderView;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_quest_view, container, false);

        title = view.findViewById(R.id.tituloPrincipal);
        reward = view.findViewById(R.id.reward);
        description = view.findViewById(R.id.txtMiddle);
        button = view.findViewById(R.id.start);

        dotsLoaderView = view.findViewById(R.id.dotsLoader);

        if(getArguments() == null)
            return view;

        Quest.Kind kind = Quest.Kind.valueOf(getArguments().getString("kind"));
        final String key = getArguments().getString("key");

        QuestHandler questHandler = null;

        boolean complete = false;

        if(kind.equals(Quest.Kind.MAIN)) {
            questHandler = MainQuestHandler.getInstance();
            complete = UserHandler.getInstance().getUser().isMainQuestComplete(key);
        } else if(kind.equals(Quest.Kind.SIDE)) {
            questHandler = SideQuestHandler.getInstance();
            complete = UserHandler.getInstance().getUser().isSideQuestComplete(key);
        }

        if(questHandler == null) {//just in case of a new quest kind
            AlertDialog.Builder responseAlert = new AlertDialog.Builder(view.getContext());
            responseAlert.setTitle("Aplicativo desatualizado!");
            responseAlert.setMessage("Por favor atualize seu aplicativo para ter suporte a esse tipo de quest!");
            responseAlert.setNeutralButton("Entendido", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    return;
                }
            });
            responseAlert.show();
            return view;
        }

        quest = questHandler.getQuest(key);

        if(quest == null) {
            AlertDialog.Builder responseAlert = new AlertDialog.Builder(view.getContext());
            responseAlert.setTitle("Ops! Algo deu Errado :(");
            responseAlert.setMessage("Não foi possivel abrir a vizualização dessa quest");
            responseAlert.setNeutralButton("Entendido", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    return;
                }
            });
            responseAlert.show();
            return view;
        }

        title.setText(quest.getTitle());
        reward.setText(String.format("%1$.0f Bitecos", quest.getReward()));
        description.setText(quest.getDescription());

        if(complete) {
            button.setBackgroundColor(Color.parseColor("#2ecc71"));
            button.setText("Completa ✓");
            return view;
        }

        if(quest.getType().equalsIgnoreCase("QRCODE")) {
            final Quest finalQuest1 = quest;
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    MainActivity.getInstance().readQRCode(new QRCodeResponse() {
                        @Override
                        public void run(IntentResult result) {
                            if(result.getContents() != null) {
                                String secret = result.getContents();

                                System.out.println(secret);
                                System.out.println(secret);
                                startLoading();

                                Call<User> call = null;
                                if(finalQuest1.getKind().equals(Quest.Kind.MAIN)) {
                                    call = new RetrofitConfig().getMainQuests().checkQRCode(new QRCodeValidation(key, secret), UserHandler.getInstance().getUserToken());

                                }else if(finalQuest1.getKind().equals(Quest.Kind.SIDE)) {
                                    call = new RetrofitConfig().getSideQuests().checkQRCode(new QRCodeValidation(key, secret), UserHandler.getInstance().getUserToken());

                                }

                                call.enqueue(new Callback<User>() {
                                    @Override
                                    public void onResponse(Call<User> call, Response<User> response) {
                                        stopLoading();
                                        if(!response.isSuccessful()) {
                                            AlertDialog.Builder responseAlert = new AlertDialog.Builder(view.getContext());
                                            responseAlert.setTitle("Temos uma fraude!");
                                            responseAlert.setMessage("Você escaneou um QRCode inválido.");
                                            responseAlert.setNeutralButton("Desculpa ;-;", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    return;
                                                }
                                            });
                                            responseAlert.show();
                                            return;
                                        }
                                        QuestComplete questComplete = new QuestComplete();

                                        Bundle bundle = new Bundle();
                                        bundle.putString("title", finalQuest1.getTitle());
                                        bundle.putDouble("reward", finalQuest1.getReward());
                                        questComplete.setArguments(bundle);

                                        MainActivity.getInstance().loadFragment(questComplete);

                                        UserHandler.getInstance().setUser(response.body());
                                    }

                                    @Override
                                    public void onFailure(Call<User> call, Throwable t) {
                                        stopLoading();
                                        AlertDialog.Builder responseAlert = new AlertDialog.Builder(view.getContext());
                                        responseAlert.setTitle("Ops! Algo deu Errado :(");
                                        responseAlert.setMessage(t.getLocalizedMessage());
                                        responseAlert.setNeutralButton("Chamarei um professor", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                return;
                                            }
                                        });
                                        responseAlert.show();
                                    }
                                });


                            }else{
                                AlertDialog.Builder responseAlert = new AlertDialog.Builder(view.getContext());
                                responseAlert.setTitle("Ops! Algo deu Errado :(");
                                responseAlert.setMessage("Não foi possivel ler o QRCode");
                                responseAlert.setNeutralButton("Entendido", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        return;
                                    }
                                });
                                responseAlert.show();
                            }
                        }
                    });
                }
            });
        }else if(quest.getType().equalsIgnoreCase("QUIZ")) {
            if(questHandler.hasQuizProgress(quest.getKey())) {
                button.setBackgroundColor(Color.parseColor("#f1c40f"));
                button.setText("Continuar");
            }else{
                button.setText("Tentar responder");
            }
            final QuestHandler finalQuestHandler = questHandler;
            final Quest finalQuest = quest;
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finalQuestHandler.getQuizProgress(finalQuest.getKey()).nextQuestion();
                }
            });
        }

        return view;
    }

    public void startLoading() {
        dotsLoaderView.show();
        title.setVisibility(View.INVISIBLE);
        reward.setVisibility(View.INVISIBLE);
        description.setVisibility(View.INVISIBLE);
        button.setVisibility(View.INVISIBLE);
    }

    public void stopLoading() {
        dotsLoaderView.hide();
        title.setVisibility(View.VISIBLE);
        reward.setVisibility(View.VISIBLE);
        description.setVisibility(View.VISIBLE);
        button.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        if(quest.getKind().equals(Quest.Kind.MAIN)) {
            MainActivity.getInstance().loadFragment(new MainQuestsFragment());
        }else if(quest.getKind().equals(Quest.Kind.SIDE)) {
            MainActivity.getInstance().loadFragment(new SideQuestsFragment());
        }
    }
}
