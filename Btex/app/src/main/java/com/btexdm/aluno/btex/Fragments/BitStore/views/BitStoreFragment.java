package com.btexdm.aluno.btex.Fragments.BitStore.views;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.btexdm.aluno.btex.Fragments.BitStore.adapters.BitStoreAdapter;
import com.btexdm.aluno.btex.Fragments.BitStore.models.BitStoreItem;
import com.btexdm.aluno.btex.Fragments.Login.Handlers.UserHandler;
import com.btexdm.aluno.btex.MainActivity;
import com.btexdm.aluno.btex.R;
import com.btexdm.aluno.btex.RetrofitConfig.RetrofitConfig;
import com.btexdm.aluno.btex.common.model.response.GenericListResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import steelkiwi.com.library.DotsLoaderView;

public class BitStoreFragment extends Fragment {

    DotsLoaderView dotsLoaderView;
    ListView productsList;

    private View view;

    // teste

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_bit_store, container, false);
        productsList = view.findViewById(R.id.productsList);
        dotsLoaderView = view.findViewById(R.id.dotsLoader);
        onLoad();
        return view;
    }
    public void onLoad(){

        dotsLoaderView.show();

        Call call = new RetrofitConfig().getStoreItens().getItens(UserHandler.getInstance().getUserToken());

        call.enqueue(new Callback<GenericListResponse<BitStoreItem>>(){

            @Override
            public void onResponse(Call<GenericListResponse<BitStoreItem>> call, Response<GenericListResponse<BitStoreItem>> response) {

                productsList.setAdapter(new BitStoreAdapter(view.getContext(), response.body().getItems().toArray(new BitStoreItem[response.body().getItems().size()])));

//                productsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                        BitStoreItem bitStoreItem = (BitStoreItem) adapterView.getItemAtPosition(i);
//
//                        BitStoreRequested bitStoreRequested = new BitStoreRequested();
//
//                        Bundle bundle = new Bundle();
//                        bundle.putInt("id", bitStoreItem.getId());
//                        bundle.putString("title", bitStoreItem.getName());
//                        bundle.putDouble("cost", bitStoreItem.getPrice());
//                        bitStoreRequested.setArguments(bundle);
//
//                        MainActivity.getInstance().loadFragment(bitStoreRequested);
//                    }
//                });
            }

            @Override
            public void onFailure(Call<GenericListResponse<BitStoreItem>> call, Throwable t) {
                AlertDialog.Builder responseAlert = new AlertDialog.Builder(view.getContext());
                responseAlert.setTitle("Ops! Algo deu Errado :(");
                responseAlert.setMessage(t.getLocalizedMessage());
                responseAlert.setNeutralButton("Chamarei um professor", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                });
                responseAlert.show();
            }
        });

        dotsLoaderView.hide();

    }
}
