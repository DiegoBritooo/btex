package com.btexdm.aluno.btex.Fragments.Quests.Models.request;

import java.util.ArrayList;
import java.util.List;

public class QuizValidation {

    private String id;

    private List<Integer> answers = new ArrayList<>();

    public QuizValidation(String id, List<Integer> answers) {
        this.id = id;
        this.answers = answers;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Integer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Integer> answers) {
        this.answers = answers;
    }
}
