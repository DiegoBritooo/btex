package com.btexdm.aluno.btex.Fragments.Quests.Models;

public enum Difficulty {

    EASY(0, "Fácil", "#32ff7e"),
    MEDIUM(1, "Médio", "#fff200"),
    HARD(2, "Dificíl", "#ff3838");

    private int id;

    private String name;
    private String color;

    Difficulty(int id, String name, String color) {
        this.id = id;
        this.name = name;
        this.color = color;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public static Difficulty getById(int id) {
        for (Difficulty difficulty : values()) {
            if(difficulty.getId() == id)
                return difficulty;
        }
        return null;
    }
}
