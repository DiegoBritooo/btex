package com.btexdm.aluno.btex.common.model;

import com.google.zxing.integration.android.IntentResult;

public interface QRCodeResponse {

    void run(IntentResult result);

}
