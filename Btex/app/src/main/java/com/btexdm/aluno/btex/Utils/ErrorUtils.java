package com.btexdm.aluno.btex.Utils;

import org.json.JSONObject;

import okhttp3.ResponseBody;

public class ErrorUtils {

    public static String getErrorMessage(ResponseBody responseBody) {
        try {
            return new JSONObject(responseBody.string()).getString("message");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

}
