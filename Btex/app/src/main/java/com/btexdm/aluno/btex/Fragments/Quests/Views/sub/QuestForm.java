package com.btexdm.aluno.btex.Fragments.Quests.Views.sub;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.btexdm.aluno.btex.Fragments.Quests.Handlers.MainQuestHandler;
import com.btexdm.aluno.btex.Fragments.Quests.Handlers.SideQuestHandler;
import com.btexdm.aluno.btex.MainActivity;
import com.btexdm.aluno.btex.R;
import com.btexdm.aluno.btex.common.model.BTeXFragment;

import steelkiwi.com.library.DotsLoaderView;

public class QuestForm extends BTeXFragment {

    private View view;

    private CardView cardView;

    private TextView title;

    private Button res_1;
    private Button res_2;
    private Button res_3;
    private Button res_4;

    private Button next;

    private Button lastChoice = null;
    private Integer answer = null;

    private DotsLoaderView dotsLoaderView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_quest_form, container, false);

        dotsLoaderView = view.findViewById(R.id.dotsLoader);

        cardView = view.findViewById(R.id.question);

        title = view.findViewById(R.id.title);

        res_1 = view.findViewById(R.id.res_1);
        res_2 = view.findViewById(R.id.res_2);
        res_3 = view.findViewById(R.id.res_3);
        res_4 = view.findViewById(R.id.res_4);

        next = view.findViewById(R.id.next);

        onLoad();

        return view;
    }

    public void onLoad(){

        title.setText(getArguments().getString("title"));

        res_1.setText(getArguments().getString("answer_0"));
        res_2.setText(getArguments().getString("answer_1"));
        res_3.setText(getArguments().getString("answer_2"));
        res_4.setText(getArguments().getString("answer_3"));

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button currentChoice = (Button) view;
                if(lastChoice != null) {
                    lastChoice.setBackgroundColor(Color.parseColor("#0DFFFFFF"));
                    if(lastChoice == currentChoice) {
                        lastChoice = null;
                        answer = null;
                        next.setBackgroundColor(Color.parseColor("#6F6969"));
                        return;
                    }
                }
                currentChoice.setBackgroundColor(Color.parseColor("#26ffffff"));
                next.setBackgroundColor(Color.parseColor("#2980b9"));
                if(view.getId() == R.id.res_1) {
                    answer = 0;
                } else if(view.getId() == R.id.res_2) {
                    answer = 1;
                } else if(view.getId() == R.id.res_3) {
                    answer = 2;
                } else if(view.getId() == R.id.res_4) {
                    answer = 3;
                }
                lastChoice = currentChoice;
            }
        };

        res_1.setOnClickListener(onClickListener);
        res_2.setOnClickListener(onClickListener);
        res_3.setOnClickListener(onClickListener);
        res_4.setOnClickListener(onClickListener);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(answer != null) {
                    String kind = getArguments().getString("kind");
                    String key = getArguments().getString("key");
                    if(kind.equalsIgnoreCase("MAIN")) {
                        MainQuestHandler.getInstance().getQuizProgress(key).registerAnswer(answer);
                    }else if(kind.equalsIgnoreCase("SIDE")) {
                        SideQuestHandler.getInstance().getQuizProgress(key).registerAnswer(answer);
                    }
                }
            }
        });

    }

    public void startLoading() {
        dotsLoaderView.show();
        title.setVisibility(View.INVISIBLE);
        res_1.setVisibility(View.INVISIBLE);
        res_2.setVisibility(View.INVISIBLE);
        res_3.setVisibility(View.INVISIBLE);
        res_4.setVisibility(View.INVISIBLE);
        next.setVisibility(View.INVISIBLE);
        cardView.setVisibility(View.INVISIBLE);
    }

    public void stopLoading() {
        dotsLoaderView.hide();
        title.setVisibility(View.VISIBLE);
        res_1.setVisibility(View.VISIBLE);
        res_2.setVisibility(View.VISIBLE);
        res_3.setVisibility(View.VISIBLE);
        res_4.setVisibility(View.VISIBLE);
        next.setVisibility(View.VISIBLE);
        cardView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        QuestView questView = new QuestView();
        Bundle bundle = new Bundle();
        bundle.putString("key", getArguments().getString("key"));
        bundle.putString("kind", getArguments().getString("kind"));
        questView.setArguments(bundle);
        MainActivity.getInstance().loadFragment(questView);
    }
}
