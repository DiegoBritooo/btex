package com.btexdm.aluno.btex.common.model.response;

import java.util.List;

public class GenericListResponse<T> {

    private int Count;
    private int ScannedCount;
    private List<T> Items;

    public int getCount() {
        return this.Count;
    }

    public String getScannedCount() {
        return String.valueOf(this.ScannedCount);
    }

    private void setCount(int count) {
        this.Count = count;
    }

    private void setScannedCount(int scannedCount) {
        this.ScannedCount = scannedCount;
    }

    public List<T> getItems() {
        return this.Items;
    }

    public void setItems(List<T> items) {
        this.Items = items;
    }
}