package com.btexdm.aluno.btex.Fragments.Login.Service;

import com.btexdm.aluno.btex.Fragments.Login.Models.LoginRequest;
import com.btexdm.aluno.btex.Fragments.Login.Models.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface LoginService {

    @POST("BTeX_Generic?type=login&act=login")
    Call<User> authenticate(@Body LoginRequest loginRequest);

}
