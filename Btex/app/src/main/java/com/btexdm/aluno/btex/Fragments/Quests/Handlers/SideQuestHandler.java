package com.btexdm.aluno.btex.Fragments.Quests.Handlers;

import com.btexdm.aluno.btex.Fragments.Quests.Handlers.pattern.QuestHandler;
import com.btexdm.aluno.btex.Fragments.Quests.Models.Quest;

public class SideQuestHandler extends QuestHandler {

    private static SideQuestHandler instance;

    public SideQuestHandler() {
        super(Quest.Kind.SIDE);
    }

    public static SideQuestHandler getInstance() {
        if(instance == null)
            instance = new SideQuestHandler();
        return instance;
    }

}
