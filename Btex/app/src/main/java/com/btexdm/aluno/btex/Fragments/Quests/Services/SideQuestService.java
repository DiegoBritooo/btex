package com.btexdm.aluno.btex.Fragments.Quests.Services;

import com.btexdm.aluno.btex.Fragments.Login.Models.User;
import com.btexdm.aluno.btex.Fragments.Quests.Models.Quest;
import com.btexdm.aluno.btex.Fragments.Quests.Models.request.QRCodeValidation;
import com.btexdm.aluno.btex.Fragments.Quests.Models.request.QuizValidation;
import com.btexdm.aluno.btex.common.model.response.GenericListResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PUT;

public interface SideQuestService {

    @GET("BTeX_Generic?type=side_quests&act=list")
    Call<GenericListResponse<Quest>> doRequest(@Header("Authorization") String token);

    @PUT("BTeX_Generic?type=side_quests&act=QRCODE")
    Call<User> checkQRCode(@Body QRCodeValidation qrCodeValidation, @Header("Authorization") String token);

    @PUT("BTeX_Generic?type=side_quests&act=QUIZ")
    Call<User> checkQuiz(@Body QuizValidation quizValidation, @Header("Authorization") String token);
}
