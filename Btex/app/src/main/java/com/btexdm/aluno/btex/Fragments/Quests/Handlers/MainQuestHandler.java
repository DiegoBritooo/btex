package com.btexdm.aluno.btex.Fragments.Quests.Handlers;

import com.btexdm.aluno.btex.Fragments.Quests.Handlers.pattern.QuestHandler;
import com.btexdm.aluno.btex.Fragments.Quests.Models.Quest;

public class MainQuestHandler extends QuestHandler {

    private static MainQuestHandler instance;

    public MainQuestHandler() {
        super(Quest.Kind.MAIN);
    }

    public static MainQuestHandler getInstance() {
        if(instance == null)
            instance = new MainQuestHandler();
        return instance;
    }

}
