package com.btexdm.aluno.btex.Fragments.Quests.Models;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

import com.btexdm.aluno.btex.Fragments.Login.Handlers.UserHandler;
import com.btexdm.aluno.btex.Fragments.Login.Models.User;
import com.btexdm.aluno.btex.Fragments.Quests.Handlers.MainQuestHandler;
import com.btexdm.aluno.btex.Fragments.Quests.Handlers.SideQuestHandler;
import com.btexdm.aluno.btex.Fragments.Quests.Models.request.QRCodeValidation;
import com.btexdm.aluno.btex.Fragments.Quests.Models.request.QuizValidation;
import com.btexdm.aluno.btex.Fragments.Quests.Views.sub.QuestComplete;
import com.btexdm.aluno.btex.Fragments.Quests.Views.sub.QuestForm;
import com.btexdm.aluno.btex.Fragments.Quests.Views.sub.QuestView;
import com.btexdm.aluno.btex.MainActivity;
import com.btexdm.aluno.btex.RetrofitConfig.RetrofitConfig;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuizProgress {

    private Quest quest;

    private List<Integer> answers = new ArrayList<>();

    private QuestForm currentForm = null;

    public QuizProgress(Quest quest) {
        this.quest = quest;
    }

    public void nextQuestion() {
        Question question = quest.getQuestions().get(answers.size());

        currentForm = new QuestForm();
        Bundle bundle = new Bundle();
        bundle.putString("title", question.getTitle());
        bundle.putString("answer_0", question.getAnswers().get(0).getText());
        bundle.putString("answer_1", question.getAnswers().get(1).getText());
        bundle.putString("answer_2", question.getAnswers().get(2).getText());
        bundle.putString("answer_3", question.getAnswers().get(3).getText());

        bundle.putString("kind", quest.getKind().name());
        bundle.putString("key", quest.getKey());

        currentForm.setArguments(bundle);

        MainActivity.getInstance().loadFragment(currentForm);
    }

    public void registerAnswer(Integer choice) {
        answers.add(choice);
        if(quest.getQuestions().size() > answers.size()) {
            nextQuestion();
        }else{
            currentForm.startLoading();

            Call<User> call = null;
            if(quest.getKind().equals(Quest.Kind.MAIN)) {
                call = new RetrofitConfig().getMainQuests().checkQuiz(new QuizValidation(quest.getKey(), answers), UserHandler.getInstance().getUserToken());

            }else if(quest.getKind().equals(Quest.Kind.SIDE)) {
                call = new RetrofitConfig().getSideQuests().checkQuiz(new QuizValidation(quest.getKey(), answers), UserHandler.getInstance().getUserToken());

            }

            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    currentForm.stopLoading();
                    if(quest.getKind().equals(Quest.Kind.MAIN)) {
                        MainQuestHandler.getInstance().unregisterProgress(quest.getKey());
                    }else if(quest.getKind().equals(Quest.Kind.SIDE)) {
                        SideQuestHandler.getInstance().unregisterProgress(quest.getKey());
                    }

                    if(!response.isSuccessful()) {
                        AlertDialog.Builder responseAlert = new AlertDialog.Builder(currentForm.getContext());
                        responseAlert.setTitle("Todo mundo erra!");
                        responseAlert.setMessage("E dessa vez foi você! alguma questão estava incorreta ;/");
                        responseAlert.setNeutralButton("Eita ;-;", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                return;
                            }
                        });
                        responseAlert.show();

                        QuestView questView = new QuestView();
                        Bundle bundle = new Bundle();
                        bundle.putString("key", quest.getKey());
                        bundle.putString("kind", quest.getKind().name());
                        questView.setArguments(bundle);

                        MainActivity.getInstance().loadFragment(questView);
                        return;
                    }
                    QuestComplete questComplete = new QuestComplete();

                    Bundle bundle = new Bundle();
                    bundle.putString("title", quest.getTitle());
                    bundle.putDouble("reward", quest.getReward());
                    questComplete.setArguments(bundle);

                    MainActivity.getInstance().loadFragment(questComplete);

                    UserHandler.getInstance().setUser(response.body());
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    currentForm.stopLoading();
                    AlertDialog.Builder responseAlert = new AlertDialog.Builder(currentForm.getContext());
                    responseAlert.setTitle("Ops! Algo deu Errado :(");
                    responseAlert.setMessage(t.getLocalizedMessage());
                    responseAlert.setNeutralButton("Chamarei um professor", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            return;
                        }
                    });
                    responseAlert.show();
                }
            });
        }
    }
}
