package com.btexdm.aluno.btex.Fragments.BitPocket.service;

import com.btexdm.aluno.btex.Fragments.BitPocket.models.AchivementList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface BitPocketService {

    @GET("BTeX_Generic?type=Achievements&act=list")
    Call<AchivementList> doRequest(@Header("Authorization") String token);

}
