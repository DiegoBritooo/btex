package com.btexdm.aluno.btex.common.model;

import android.support.v4.app.Fragment;
import android.view.View;

public abstract class BTeXFragment extends Fragment {

    public abstract void onBackPressed();

}
