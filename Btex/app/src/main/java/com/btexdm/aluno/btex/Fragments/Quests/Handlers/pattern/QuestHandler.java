package com.btexdm.aluno.btex.Fragments.Quests.Handlers.pattern;

import com.btexdm.aluno.btex.Fragments.Quests.Models.Quest;
import com.btexdm.aluno.btex.Fragments.Quests.Models.QuizProgress;

import java.util.HashMap;

public abstract class QuestHandler {

    private Quest.Kind kind;

    private HashMap<String, Quest> quests = new HashMap<>();
    private HashMap<String, QuizProgress> quizProgress = new HashMap<>();

    public QuestHandler(Quest.Kind kind) {
        this.kind = kind;
    }

    public void registerQuest(Quest... quests) {
        for (Quest quest : quests) {
            quest.setKind(kind);
            this.quests.put(quest.getKey(), quest);
        }
    }

    public boolean hasQuizProgress(String key) {
        return quizProgress.containsKey(key);
    }

    public QuizProgress getQuizProgress(String key) {
        Quest quest = getQuest(key);
        if(quest != null) {
            if(!quizProgress.containsKey(key))
                quizProgress.put(key, new QuizProgress(quest));
            return quizProgress.get(key);
        }
        return null;
    }

    public Quest getQuest(String key) {
        if(quests.containsKey(key))
            return quests.get(key);
        return null;
    }

    public void unregisterProgress(String key) {
        quizProgress.remove(key);
    }
}
