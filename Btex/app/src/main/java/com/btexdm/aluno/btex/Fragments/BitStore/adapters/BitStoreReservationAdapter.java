package com.btexdm.aluno.btex.Fragments.BitStore.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.btexdm.aluno.btex.Fragments.BitStore.models.BitStoreItem;
import com.btexdm.aluno.btex.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;

public class BitStoreReservationAdapter extends ArrayAdapter<BitStoreItem> {

    private final Context context;
    private final BitStoreItem[] values;

    public BitStoreReservationAdapter(Context context, BitStoreItem[] values) {
        super(context, R.layout.item_bit_store_reservation, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.item_bit_store_reservation, parent, false);

        BitStoreItem bitStore = values[position];

        TextView title = (TextView) rowView.findViewById(R.id.productTitle);
        ImageView productImg = (ImageView) rowView.findViewById(R.id.productImg);
        TextView reservationDate = (TextView) rowView.findViewById(R.id.reservationDate);

        Picasso.get().load(bitStore.getIcon()).into(productImg);

        title.setText(bitStore.getName());

        DecimalFormat formatter = new DecimalFormat("#,###");
        reservationDate.setText(formatter.format(bitStore.getPrice()) + " Bitecos");

        return rowView;
    }
}
