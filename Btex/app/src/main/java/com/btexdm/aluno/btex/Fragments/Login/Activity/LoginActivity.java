package com.btexdm.aluno.btex.Fragments.Login.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.btexdm.aluno.btex.Fragments.Login.Handlers.UserHandler;
import com.btexdm.aluno.btex.Fragments.Login.Models.LoginRequest;
import com.btexdm.aluno.btex.Fragments.Login.Models.User;
import com.btexdm.aluno.btex.MainActivity;
import com.btexdm.aluno.btex.R;
import com.btexdm.aluno.btex.RetrofitConfig.RetrofitConfig;
import com.btexdm.aluno.btex.Utils.ErrorUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import steelkiwi.com.library.DotsLoaderView;

public class LoginActivity extends AppCompatActivity {

    EditText inpUsername;
    EditText inpPassword;
    ImageView logo;

    Button btnLogin;

    DotsLoaderView dotsLoaderView;
    LoginRequest loginRequest;

    SharedPreferences preferences;
    Switch remember;
    TextView rememberText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        inpUsername = findViewById(R.id.inptUser);
        inpPassword = findViewById(R.id.inptPassword);
        dotsLoaderView = findViewById(R.id.dotsLoader);
        logo = findViewById(R.id.logo);
        btnLogin = findViewById(R.id.btnLogin);
        remember = findViewById(R.id.switchBtn);
        rememberText = findViewById(R.id.rememberText);

        preferences =  getSharedPreferences("BTEX", MODE_PRIVATE);

        String user = preferences.getString("USER","");
        String passwd = preferences.getString("PASSWD","");

        inpUsername.setText(user);
        inpPassword.setText(passwd);

        if (!user.isEmpty() && !passwd.isEmpty()){
            scheduleLogin();
        }
    }

    public void tryLogin(View view) {
        scheduleLogin();
    }

    private void scheduleLogin() {

        inpUsername.setVisibility(View.INVISIBLE);
        inpPassword.setVisibility(View.INVISIBLE);
        logo.setVisibility(View.INVISIBLE);
        btnLogin.setVisibility(View.INVISIBLE);
        remember.setVisibility(View.INVISIBLE);
        rememberText.setVisibility(View.INVISIBLE);

        dotsLoaderView.show();

        loginRequest = new LoginRequest(inpUsername.getText().toString(), inpPassword.getText().toString());

        Call<User> call = new RetrofitConfig().getTokens().authenticate(loginRequest);

        call.enqueue(new Callback<User>() {

            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                System.out.println(response.code());

                User user = response.body();

                if (response.code() != 200) {

                    AlertDialog.Builder responseAlert = new AlertDialog.Builder(LoginActivity.this);
                    responseAlert.setTitle("Ops! Algo deu Errado :(");
                    responseAlert.setMessage(ErrorUtils.getErrorMessage(response.errorBody()));
                    responseAlert.setNeutralButton("Entendido", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            return;
                        }
                    });

                    responseAlert.show();

                    inpUsername.setVisibility(View.VISIBLE);
                    inpPassword.setVisibility(View.VISIBLE);
                    logo.setVisibility(View.VISIBLE);
                    btnLogin.setVisibility(View.VISIBLE);
                    remember.setVisibility(View.VISIBLE);
                    rememberText.setVisibility(View.VISIBLE);

                } else {

                    if(remember.isChecked())
                        saveData();

                    UserHandler.getInstance().setUser(user);

                    Intent mainActivity = new Intent(LoginActivity.this, MainActivity.class);

                    startActivity(mainActivity);
                }
                dotsLoaderView.hide();
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

                AlertDialog.Builder alertFailure = new AlertDialog.Builder(LoginActivity.this);

                alertFailure.setTitle("Ops! Algo deu Errado");
                alertFailure.setMessage("Algo deu errado e não pude consertar, Sorry :(");
                alertFailure.setNeutralButton("Entendido", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                });

                dotsLoaderView.hide();
                inpUsername.setVisibility(View.VISIBLE);
                inpPassword.setVisibility(View.VISIBLE);
                logo.setVisibility(View.VISIBLE);
                btnLogin.setVisibility(View.VISIBLE);
                remember.setVisibility(View.VISIBLE);
                rememberText.setVisibility(View.VISIBLE);
            }
        });
    }

    public void saveData(){

        SharedPreferences.Editor edit = preferences.edit();

        edit.putString("USER", inpUsername.getText().toString());
        edit.putString("PASSWD", inpPassword.getText().toString());

        edit.apply();
    }

}
