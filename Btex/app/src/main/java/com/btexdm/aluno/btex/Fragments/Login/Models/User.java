package com.btexdm.aluno.btex.Fragments.Login.Models;

import com.btexdm.aluno.btex.Fragments.BitStore.models.BitStoreItem;

import java.util.List;

public class User {

    private String Token;
    private String nome;
    private String RA;
    private String turma;
    private String message;
    private String foto;
    private Double biteco;

    private UserCompleteQuests complete;

    private List<UserAchievement> achievements;

    private List<BitStoreItem> myItems;

    public UserCompleteQuests getComplete() {
        return complete;
    }

    public void setComplete(UserCompleteQuests complete) {
        this.complete = complete;
    }

    public boolean isMainQuestComplete(String key) {
        if(complete != null && complete.getMAIN_QUESTS() != null) {
            for (UserCompleteQuest mainQuest : complete.getMAIN_QUESTS()) {
                if(mainQuest.getId().equalsIgnoreCase(key))
                    return true;
            }

        }
        return false;
    }

    public boolean isSideQuestComplete(String key) {
        if(complete != null && complete.getSIDE_QUESTS() != null) {
            for (UserCompleteQuest sideQuest : complete.getSIDE_QUESTS()) {
                if(sideQuest.getId().equalsIgnoreCase(key))
                    return true;
            }

        }
        return false;
    }

    public boolean isAchievementComplete(String key) {
        if(achievements != null) {
            for (UserAchievement achievement : achievements) {
                if(achievement.getId().equalsIgnoreCase(key))
                    return true;
            }

        }
        return false;
    }

    public boolean isItemInWallet(String key) {
        if(myItems != null) {
            for(BitStoreItem bitStoreItem : myItems) {
                if(bitStoreItem.getKey().equalsIgnoreCase(key))
                    return true;
            }
        }
        return false;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getRA() {
        return RA;
    }

    public void setRA(String RA) {
        this.RA = RA;
    }

    public String getTurma() {
        return turma;
    }

    public void setTurma(String turma) {
        this.turma = turma;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public Double getBiteco() {
        return biteco;
    }

    public void setBiteco(Double biteco) {
        this.biteco = biteco;
    }

    public List<BitStoreItem> getMyItems() {
        return myItems;
    }

    public void setMyItems(List<BitStoreItem> myItems) {
        this.myItems = myItems;
    }

    public List<UserAchievement> getAchievements() {
        return achievements;
    }

    public void setAchievements(List<UserAchievement> achievements) {
        this.achievements = achievements;
    }
}
