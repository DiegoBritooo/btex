package com.btexdm.aluno.btex.Fragments.BitStore.adapters;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.btexdm.aluno.btex.Fragments.BitPocket.BitPocket;
import com.btexdm.aluno.btex.Fragments.BitStore.models.BitStoreItem;
import com.btexdm.aluno.btex.Fragments.BitStore.views.BitStoreRequested;
import com.btexdm.aluno.btex.Fragments.Login.Handlers.UserHandler;
import com.btexdm.aluno.btex.MainActivity;
import com.btexdm.aluno.btex.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;

public class BitStoreAdapter extends ArrayAdapter<BitStoreItem> {

    private final Context context;
    private final BitStoreItem[] values;

    public BitStoreAdapter(Context context, BitStoreItem[] values) {
        super(context, R.layout.item_bit_store, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.item_bit_store, parent, false);

        final BitStoreItem bitStore = values[position];

        TextView title = (TextView) rowView.findViewById(R.id.productTitle);
        TextView productValue = (TextView) rowView.findViewById(R.id.productValue);
        ImageView productImg = (ImageView) rowView.findViewById(R.id.productImg);

        Button buy = rowView.findViewById(R.id.btnComprar);

        Picasso.get().load(bitStore.getIcon()).into(productImg);

        title.setText(bitStore.getName());

        DecimalFormat formatter = new DecimalFormat("#,###");
        productValue.setText(formatter.format(bitStore.getPrice()) + " Bitecos");

        if(UserHandler.getInstance().getUser().isItemInWallet(bitStore.getKey())) {
            buy.setBackgroundColor(Color.parseColor("#FF5555"));
            buy.setText("Comprado ✓");
        }else{
            buy.setOnClickListener(new AdapterView.OnClickListener() {
                @Override
                public void onClick(View view) {
                    BitStoreRequested bitStoreRequested = new BitStoreRequested();

                    Bundle bundle = new Bundle();
                    bundle.putString("key", bitStore.getKey());
                    bundle.putString("title", bitStore.getName());
                    bundle.putDouble("cost", bitStore.getPrice());
                    bitStoreRequested.setArguments(bundle);

                    MainActivity.getInstance().loadFragment(bitStoreRequested);
                }
            });
        }

        return rowView;
    }
}
