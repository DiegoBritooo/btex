package com.btexdm.aluno.btex.Fragments.BitStore.services;

import com.btexdm.aluno.btex.Fragments.BitStore.models.BitStoreItem;
import com.btexdm.aluno.btex.Fragments.BitStore.models.BuyItemAction;
import com.btexdm.aluno.btex.Fragments.Login.Models.User;
import com.btexdm.aluno.btex.common.model.response.GenericListResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface BitStoreService {

    @GET("BTeX_Generic?type=shop&act=list")
    Call<GenericListResponse<BitStoreItem>> getItens(@Header("Authorization") String token);


    @PUT("BTeX_Generic?type=shop&act=shop")
    Call<User> buy(@Header("Authorization") String token, @Body BuyItemAction buyItemAction);

}
