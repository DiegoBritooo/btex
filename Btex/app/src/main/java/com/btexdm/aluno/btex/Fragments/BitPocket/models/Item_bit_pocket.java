package com.btexdm.aluno.btex.Fragments.BitPocket.models;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.btexdm.aluno.btex.Fragments.BitPocket.BitPocket;
import com.btexdm.aluno.btex.MainActivity;
import com.btexdm.aluno.btex.R;
import com.btexdm.aluno.btex.common.model.BTeXFragment;
import com.squareup.picasso.Picasso;

import steelkiwi.com.library.DotsLoaderView;


public class Item_bit_pocket extends BTeXFragment {

    DotsLoaderView dotsLoaderView;
    private View view;

    TextView title;
    TextView description;
    ImageView icon;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_item_bit_pocket, container, false);

        title = view.findViewById(R.id.itemTitle);
        description = view.findViewById(R.id.description);
        icon = view.findViewById(R.id.imgItem);

        Bundle bundle = getArguments();

        title.setText(bundle.getString("title"));
        description.setText(bundle.getString("description"));
        Picasso.get().load(bundle.getString("icon")).into(icon);

        onLoad();
        return view;
    }

    public void onLoad() {

    }

    @Override
    public void onBackPressed() {
        MainActivity.getInstance().loadFragment(new BitPocket());
    }
}
