package com.btexdm.aluno.btex.Fragments.Quests.Views;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;


import com.btexdm.aluno.btex.Fragments.Login.Handlers.UserHandler;
import com.btexdm.aluno.btex.Fragments.Quests.Adapters.MainQuestsAdapter;
import com.btexdm.aluno.btex.Fragments.Quests.Handlers.MainQuestHandler;
import com.btexdm.aluno.btex.Fragments.Quests.Models.Quest;
import com.btexdm.aluno.btex.Fragments.Quests.Views.sub.QuestView;
import com.btexdm.aluno.btex.MainActivity;
import com.btexdm.aluno.btex.R;
import com.btexdm.aluno.btex.RetrofitConfig.RetrofitConfig;
import com.btexdm.aluno.btex.common.model.response.GenericListResponse;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import steelkiwi.com.library.DotsLoaderView;

public class MainQuestsFragment extends Fragment {

    DotsLoaderView dotsLoaderView;

    private View view;

    private ListView questList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_main_quests, container, false);
        questList = view.findViewById(R.id.questList);
        dotsLoaderView = view.findViewById(R.id.dotsLoader);
        onLoad();
        return view;
    }



    public void onLoad(){

        dotsLoaderView.show();

        Call<GenericListResponse<Quest>> call = new RetrofitConfig().getMainQuests().listQuests(UserHandler.getInstance().getUserToken());

        call.enqueue(new Callback<GenericListResponse<Quest>>(){

            @Override
            public void onResponse(Call<GenericListResponse<Quest>> call, Response<GenericListResponse<Quest>> response) {
                final Quest[] quests = response.body().getItems().toArray(new Quest[response.body().getItems().size()]);
                MainQuestHandler.getInstance().registerQuest(quests);

                dotsLoaderView.hide();

                questList.setAdapter(new MainQuestsAdapter(view.getContext(), quests));
                questList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Quest quest = (Quest) adapterView.getItemAtPosition(i);

                        QuestView questView = new QuestView();
                        Bundle bundle = new Bundle();
                        bundle.putString("key", quest.getKey());
                        bundle.putString("kind", quest.getKind().name());
                        questView.setArguments(bundle);

                        MainActivity.getInstance().loadFragment(questView);
                    }
                });
            }

            @Override
            public void onFailure(Call<GenericListResponse<Quest>> call, Throwable t) {
                AlertDialog.Builder responseAlert = new AlertDialog.Builder(view.getContext());
                responseAlert.setTitle("Ops! Algo deu Errado :(");
                responseAlert.setMessage(t.getLocalizedMessage());
                responseAlert.setNeutralButton("Chamarei um professor", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                });
                responseAlert.show();
                dotsLoaderView.hide();
            }
        });


    }


}
