package com.btexdm.aluno.btex.RetrofitConfig;


import com.btexdm.aluno.btex.Fragments.BitPocket.BitPocket;
import com.btexdm.aluno.btex.Fragments.BitPocket.service.BitPocketService;
import com.btexdm.aluno.btex.Fragments.BitStore.services.BitStoreService;
import com.btexdm.aluno.btex.Fragments.Login.Service.LoginService;
import com.btexdm.aluno.btex.Fragments.Quests.Services.MainQuestService;
import com.btexdm.aluno.btex.Fragments.Quests.Services.SideQuestService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitConfig {

    private final Retrofit retrofit;

    public RetrofitConfig() {

        this.retrofit = new Retrofit.Builder().baseUrl("https://w1qzp10rte.execute-api.us-east-1.amazonaws.com/default/").addConverterFactory(GsonConverterFactory.create())
                .build();

    }

    public LoginService getTokens() {

        return this.retrofit.create(LoginService.class);

    }

    public MainQuestService getMainQuests() {
        return this.retrofit.create(MainQuestService.class);
    }

    public SideQuestService getSideQuests(){ return this.retrofit.create(SideQuestService.class);}

    public BitStoreService getStoreItens(){return this.retrofit.create(BitStoreService.class);}

    public BitPocketService getAchievements(){return this.retrofit.create(BitPocketService.class);}
}
