package com.btexdm.aluno.btex.Fragments.BitPocket.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.btexdm.aluno.btex.Fragments.BitPocket.models.Achievement;
import com.btexdm.aluno.btex.Fragments.Login.Handlers.UserHandler;
import com.btexdm.aluno.btex.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class BitPocketAchievementAdapter extends BaseAdapter {

    Context context;
    View view;
    LayoutInflater layoutInflater;
    private Achievement[] achievements;

    public BitPocketAchievementAdapter(Context context, Achievement[] achievements) {
        this.achievements = achievements;
        this.context = context;
    }

    @Override
    public int getCount() {
        return achievements.length;
    }

    @Override
    public Object getItem(int position) {
        return achievements[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Achievement achievement = this.achievements[position];

        if (convertView == null) {

            view = new View(context);

            view = layoutInflater.inflate(R.layout.item_bit_pocket, null);

            ImageView imageView = (ImageView) view.findViewById(R.id.imgPocket);
            TextView textView = (TextView) view.findViewById(R.id.textPocket);

            if(UserHandler.getInstance().getUser().isAchievementComplete(achievement.getKey())) {
                Picasso.get().load(achievement.getIcon()).into(imageView);
                imageView.setScaleType(ImageView.ScaleType.FIT_START);
                textView.setText(achievement.getTitle());
                textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            }else{
                Picasso.get().load("https://i.imgur.com/V6pUsro.png").into(imageView);
                textView.setText("???");
                textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            }
        }

        return view;
    }
}
