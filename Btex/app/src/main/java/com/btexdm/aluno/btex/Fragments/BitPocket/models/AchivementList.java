package com.btexdm.aluno.btex.Fragments.BitPocket.models;

import java.util.List;

public class AchivementList {

    private List<Achievement> Items;

    public AchivementList() {
    }

    public AchivementList(List<Achievement> items) {
        Items = items;
    }

    public List<Achievement> getItems() {
        return Items;
    }

    public void setItems(List<Achievement> items) {
        Items = items;
    }
}
