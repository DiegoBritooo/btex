package com.btexdm.aluno.btex.Fragments.Quests.Models.request;

import com.btexdm.aluno.btex.Fragments.Login.Models.User;

import retrofit2.Call;

public class QRCodeValidation {

    private String id;

    private String secret;

    public QRCodeValidation(String key, String secret) {
        this.id = key;
        this.secret = secret;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
