package com.btexdm.aluno.btex.Fragments.Quests.Views.sub;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.btexdm.aluno.btex.Fragments.Quests.Views.MainQuestsFragment;
import com.btexdm.aluno.btex.MainActivity;
import com.btexdm.aluno.btex.R;
import com.btexdm.aluno.btex.common.model.BTeXFragment;

import steelkiwi.com.library.DotsLoaderView;

public class QuestComplete extends BTeXFragment {

    private View view;

    private TextView description;
    private TextView reward;

    private Button start;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_quest_complete, container, false);

        description = view.findViewById(R.id.txtMiddle);
        reward = view.findViewById(R.id.reward);

        start = view.findViewById(R.id.start);

        onLoad();
        return view;
    }


    public void onLoad() {
        description.setText("Parabéns, você concluiu '" + getArguments().getString("title") + "'");
        reward.setText(String.format("Ganhou + %.0f bitecos", getArguments().getDouble("reward")));

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.getInstance().loadFragment(new MainQuestsFragment());
            }
        });
    }

    @Override
    public void onBackPressed() {
        MainActivity.getInstance().loadFragment(new MainQuestsFragment());
    }
}
