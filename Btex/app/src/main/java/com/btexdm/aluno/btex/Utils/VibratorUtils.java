package com.btexdm.aluno.btex.Utils;

import android.content.Context;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;

import com.btexdm.aluno.btex.MainActivity;

public class VibratorUtils {

    public static void vibrate(int duration) {
        if(MainActivity.getInstance() != null) {
            Vibrator v = (Vibrator) MainActivity.getInstance().getSystemService(Context.VIBRATOR_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                v.vibrate(VibrationEffect.createOneShot(duration, VibrationEffect.DEFAULT_AMPLITUDE));
            } else {
                //deprecated in API 26
                v.vibrate(duration);
            }
        }
    }
}
