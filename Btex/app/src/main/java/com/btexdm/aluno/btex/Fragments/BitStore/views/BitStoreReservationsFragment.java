package com.btexdm.aluno.btex.Fragments.BitStore.views;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.btexdm.aluno.btex.Fragments.BitStore.adapters.BitStoreReservationAdapter;
import com.btexdm.aluno.btex.Fragments.BitStore.models.BitStoreItem;
import com.btexdm.aluno.btex.Fragments.Login.Handlers.UserHandler;
import com.btexdm.aluno.btex.Fragments.Quests.Models.Quest;
import com.btexdm.aluno.btex.R;



public class BitStoreReservationsFragment extends Fragment {

    private View view;

    private ListView listView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_bit_store_reservations, container, false);

        listView = view.findViewById(R.id.productsList);

        if(UserHandler.getInstance().getUser() != null && UserHandler.getInstance().getUser().getMyItems() != null && UserHandler.getInstance().getUser().getMyItems().size() > 0) {

            BitStoreItem[] bitStoreItems = UserHandler.getInstance().getUser().getMyItems().toArray(new BitStoreItem[UserHandler.getInstance().getUser().getMyItems().size()]);

            listView.setAdapter(new BitStoreReservationAdapter(view.getContext(), bitStoreItems));
        }else{
            //TODO por no background q ele ainda nao tem nenhum item reservado
        }

        return view;
    }
}
