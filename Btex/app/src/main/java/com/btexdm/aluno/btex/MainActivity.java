package com.btexdm.aluno.btex;

import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.btexdm.aluno.btex.Fragments.BitPocket.BitPocket;
import com.btexdm.aluno.btex.Fragments.BitStore.views.BitStoreFragment;
import com.btexdm.aluno.btex.Fragments.BitStore.views.BitStoreReservationsFragment;
import com.btexdm.aluno.btex.Fragments.Login.Activity.LoginActivity;
import com.btexdm.aluno.btex.Fragments.Login.Handlers.UserHandler;
import com.btexdm.aluno.btex.Fragments.QrCode.QrCodeFragment;
import com.btexdm.aluno.btex.Fragments.Quests.Views.MainQuestsFragment;
import com.btexdm.aluno.btex.Fragments.Quests.Views.SideQuestsFragment;
import com.btexdm.aluno.btex.common.model.BTeXFragment;
import com.btexdm.aluno.btex.common.model.QRCodeResponse;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static MainActivity instance;

    private QRCodeResponse currentQRCode;

    private TextView name;
    private TextView studentClass;
    private TextView RA;
    private ImageView photo;

    private TextView bitecos;

    private Fragment currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        instance = this;

        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(null);

        bitecos = findViewById(R.id.bitecos);
        setBitecos(UserHandler.getInstance().getUser().getBiteco());

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (savedInstanceState == null) {
            loadFragment(new MainQuestsFragment());
            navigationView.setCheckedItem(R.id.nav_main_quests);
        }
    }

    @Override
    public void onBackPressed() {
        if(currentFragment != null && currentFragment instanceof BTeXFragment) {
            ((BTeXFragment) currentFragment).onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        name = findViewById(R.id.idName);
        studentClass = findViewById(R.id.idClass);
        photo = findViewById(R.id.idPhoto);

        Picasso.get().load(UserHandler.getInstance().getUser().getFoto()).into(photo);

        name.setText(UserHandler.getInstance().getUser().getNome());
        studentClass.setText(UserHandler.getInstance().getUser().getTurma());
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_main_quests) {
            loadFragment(new MainQuestsFragment());
        } else if (id == R.id.nav_side_quests) {
            loadFragment(new SideQuestsFragment());
        } else if (id == R.id.nav_bit_pocket) {
            loadFragment(new BitPocket());
        } else if (id == R.id.nav_bit_store) {
            loadFragment(new BitStoreFragment());
        } else if (id == R.id.nav_bit_store_reservations) {
            loadFragment(new BitStoreReservationsFragment());
        }else if (id == R.id.nav_qrcode) {
                loadFragment(new QrCodeFragment());
        } else if (id == R.id.nav_logout) {

            SharedPreferences preferences = getSharedPreferences("BTEX", MODE_PRIVATE);
            SharedPreferences.Editor edit = preferences.edit();
            edit.clear();
            edit.apply();

            UserHandler.getInstance().setUser(null);
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void loadFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
        currentFragment = fragment;
    }

    public void readQRCode(QRCodeResponse qrCodeResponse) {
        currentQRCode = qrCodeResponse;

        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        integrator.setCameraId(0);
        integrator.initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(currentQRCode != null) {
            currentQRCode.run(result);
            currentQRCode = null;
        }else{
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void setBitecos(Double biteco) {
        DecimalFormat formatter = new DecimalFormat("#,###");
        bitecos.setText(formatter.format(biteco) + " BTS");
    }

    public static MainActivity getInstance() {
        return instance;
    }
}
