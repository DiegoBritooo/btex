package com.btexdm.aluno.btex.Fragments.QrCode;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.btexdm.aluno.btex.Fragments.Quests.Views.MainQuestsFragment;
import com.btexdm.aluno.btex.MainActivity;
import com.btexdm.aluno.btex.R;
import com.btexdm.aluno.btex.common.model.BTeXFragment;

public class QrCodeFragment extends BTeXFragment {


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_qr_code, container, false);
    }

    @Override
    public void onBackPressed() {
        MainActivity.getInstance().loadFragment(new MainQuestsFragment());
    }
}
