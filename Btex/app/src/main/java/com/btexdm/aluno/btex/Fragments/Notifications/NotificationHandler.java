package com.btexdm.aluno.btex.Fragments.Notifications;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RemoteViews;

import com.btexdm.aluno.btex.Fragments.BitPocket.adapters.BitPocketAchievementAdapter;
import com.btexdm.aluno.btex.Fragments.BitPocket.models.Achievement;
import com.btexdm.aluno.btex.Fragments.BitPocket.models.AchivementList;
import com.btexdm.aluno.btex.Fragments.BitPocket.models.Item_bit_pocket;
import com.btexdm.aluno.btex.Fragments.Login.Handlers.UserHandler;
import com.btexdm.aluno.btex.Fragments.Login.Models.UserAchievement;
import com.btexdm.aluno.btex.MainActivity;
import com.btexdm.aluno.btex.R;
import com.btexdm.aluno.btex.RetrofitConfig.RetrofitConfig;
import com.btexdm.aluno.btex.Utils.ErrorUtils;
import com.btexdm.aluno.btex.Utils.VibratorUtils;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationHandler {

    public static void achievementNotification(final UserAchievement userAchievement) {
        if(MainActivity.getInstance() != null) {

            Call<AchivementList> call = new RetrofitConfig().getAchievements().doRequest(UserHandler.getInstance().getUserToken());

            call.enqueue(new Callback<AchivementList>() {

                @Override
                public void onResponse(Call<AchivementList> call, Response<AchivementList> response) {

                    for (Achievement item : response.body().getItems()) {
                        if(item.getKey().equalsIgnoreCase(userAchievement.getId())) {

                            NotificationManager mNotificationManager = (NotificationManager) MainActivity.getInstance().getSystemService(Context.NOTIFICATION_SERVICE);

                            long[] vibration = new long[1];
                            vibration[0] = 500l;

                            Notification notification = new NotificationCompat.Builder(MainActivity.getInstance())
                                    .setSmallIcon(R.mipmap.logo)
                                    .setLargeIcon(BitmapFactory.decodeResource(MainActivity.getInstance().getResources(), R.mipmap.question_mark))
                                    .setVibrate(vibration)
                                    .setContentTitle(item.getTitle())
                                    .setContentText("Você alcançou uma nova conquista!")
                                    .build();

                            int nid = new Random().nextInt(1000);
                            mNotificationManager.notify(nid, notification);
                        }
                    }
                }


                @Override
                public void onFailure(Call<AchivementList> call, Throwable t) {

                }
            });

        }

    }

}
