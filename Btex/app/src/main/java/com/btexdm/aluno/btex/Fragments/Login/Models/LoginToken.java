package com.btexdm.aluno.btex.Fragments.Login.Models;

import java.io.Serializable;

public class LoginToken implements Serializable {

    private String token;
    private String nome;
    private String RA;
    private String turma;
    private String message;
    private String foto;
    private String biteco;

    public LoginToken() {
    }

    public LoginToken(String token, String nome, String RA, String turma, String message, String foto, String biteco) {
        this.token = token;
        this.nome = nome;
        this.RA = RA;
        this.turma = turma;
        this.message = message;
        this.foto = foto;
        this.biteco = biteco;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getRA() {
        return RA;
    }

    public void setRA(String RA) {
        this.RA = RA;
    }

    public String getTurma() {
        return turma;
    }

    public void setTurma(String turma) {
        this.turma = turma;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getBiteco() {
        return biteco;
    }

    public void setBiteco(String biteco) {
        this.biteco = biteco;
    }

    public void clearTokens(){
        this.token = null;
        this.nome = null;
        this.RA = null;
        this.turma = null;
        this.message = null;
    }
	
}
