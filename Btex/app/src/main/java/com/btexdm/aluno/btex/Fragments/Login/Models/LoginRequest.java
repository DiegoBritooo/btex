package com.btexdm.aluno.btex.Fragments.Login.Models;

public class LoginRequest {

    private String RA;
    private String password;

    public LoginRequest(String RA, String password) {
        this.RA = RA;
        this.password = password;
    }

    public String getRa() {
        return RA;
    }

    public void setRa(String ra) {
        this.RA = ra;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
