package com.btexdm.aluno.btex.Fragments.Quests.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.btexdm.aluno.btex.Fragments.Login.Handlers.UserHandler;
import com.btexdm.aluno.btex.Fragments.Quests.Models.Quest;
import com.btexdm.aluno.btex.R;

public class SideQuestAdapter extends ArrayAdapter<Quest> {

    private final Context context;
    private final Quest[] values;

    public SideQuestAdapter(Context context, Quest[] values) {
        super(context, R.layout.item_side_quest, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_side_quest, parent, false);

        Quest sideQuest = values[position];

        TextView title = (TextView) rowView.findViewById(R.id.title);
        TextView description = (TextView) rowView.findViewById(R.id.description);
        TextView difficulty = (TextView) rowView.findViewById(R.id.difficulty);
        ImageView done = rowView.findViewById(R.id.questFinished);

        ConstraintLayout layout = rowView.findViewById(R.id.layout);

        if(UserHandler.getInstance().getUser().isSideQuestComplete(sideQuest.getKey())) {
            done.setVisibility(View.VISIBLE);
            title.setPadding(60,0,0,0);
        }

        title.setText(sideQuest.getTitle());
        description.setText(sideQuest.getDescription());

        difficulty.setText(sideQuest.getDifficultyEnum().getName());
        difficulty.setTextColor(Color.parseColor(sideQuest.getDifficultyEnum().getColor()));
        difficulty.setAlpha((float) 0.6);
        return rowView;
    }

}
